<?php

namespace Orchestra\Action\Contracts;

interface AsController extends Action
{
   public function authorize(): bool;

   public function beforeValidation();

   public function validate();

   public function afterValidation();
}
