<?php

namespace Orchestra\Action\Contracts;

interface Action
{
   public function createFrom($class): object;

   public function handle();
}
