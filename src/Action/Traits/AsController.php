<?php

namespace Orchestra\Action\Traits;

use Orchestra\Exceptions\UnauthorizedAction;

trait AsController
{
   use Action;

   public function authorize(): bool
   {
      return true;
   }

   public function beforeValidation()
   {
   }

   public function validate()
   {
   }

   public function afterValidation()
   {
   }

   public function __invoke($attributes = [])
   {
      $this->attributes = empty($this->attributes) ? $attributes : array_merge_recursive($this->attributes, $attributes);

      if (!$this->authorize()) {
         throw new UnauthorizedAction("Unauthorized action called", 401);
      }

      $this->runValidation();

      return $this->handle();
   }

   private function runValidation()
   {
      $this->beforeValidation();

      $this->validate();

      $this->afterValidation();
   }
}
