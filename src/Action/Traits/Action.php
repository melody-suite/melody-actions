<?php

namespace Orchestra\Action\Traits;

use Orchestra\Action\Contracts\Action as ContractsAction;
use Orchestra\Exceptions\InvalidActionException;

trait Action
{
   protected $attributes = [];

   public function __construct($attributes = [])
   {
      $this->attributes = $attributes;
   }

   public function __set($name, $value)
   {
      $this->attributes[$name] = $value;
   }

   public function __get($name)
   {
      return empty($this->attributes[$name]) ? null : $this->attributes[$name];
   }

   public function run($attributes = [])
   {
      $this->attributes = empty($this->attributes) ? $attributes : array_merge_recursive($this->attributes, $attributes);

      return $this->handle();
   }

   public function createFrom($class): object
   {
      $instance = new $class($this->attributes);

      if (!($instance instanceof ContractsAction)) {
         throw new InvalidActionException("$class is not a valid Action");
      }

      return $instance;
   }
}
